﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer
{
    [Serializable]
    public class PlayerData
    {
        public string username;
        public string password;

        public PlayerData(PlayerData data)
        {
            username = data.username;
            password = data.password;
        }
    }
}
