﻿using System;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace GameServer
{
    class Server
    {
        static int port = 8080;
        public Thread thread;
        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
        UdpClient listener = new UdpClient(port);

        string message;
        PlayerData playerData;

        public void Start()
        {
            thread = new Thread(StartListen);
            thread.Start();

            Console.WriteLine("Server Start\n");
        }

        public void StartListen()
        {
            while (true)
            {
                byte[] data = listener.Receive(ref sender);

                Console.WriteLine("Received message from " + sender);

                

                if (SerializeSystem.Deserialize<string>(data) != null)
                {
                    string recv = SerializeSystem.Deserialize<string>(data);
                    message = (string)recv;
                    Console.WriteLine(message);

                    data = SerializeSystem.Serialize(message);
                    listener.Send(data, data.Length, sender);
                }
                else
                {
                    PlayerData recv = new PlayerData(SerializeSystem.Deserialize<PlayerData>(data));

                    playerData = (PlayerData)recv;
                    message = "Login Success";
                    Console.WriteLine(message);

                    data = SerializeSystem.Serialize(message);
                    listener.Send(data, data.Length, sender);
                }
            }
        }
    }
}
