﻿[System.Serializable]
public class PlayerData
{
    public string username;
    public string password;

    public PlayerData(string username, string password)
    {
        this.username = username;
        this.password = password;
    }
}
