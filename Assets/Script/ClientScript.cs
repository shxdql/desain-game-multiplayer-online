﻿using UnityEngine;
using System.Net;
using System.Net.Sockets;

public class ClientScript : MonoBehaviour
{
    IPEndPoint ipEP, sender;
    Socket socket;

    string message;
    byte[] data = new byte[4096];

    private void Start()
    {
        sender = new IPEndPoint(IPAddress.Any, 0);
    }

    public void ConnectToServer(string ip, int port)
    {
        ipEP = new IPEndPoint(IPAddress.Parse(ip), port);
        socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

        string mess = "Connect to Server";
        data = SerializeSystem.SerializeData(mess);
        socket.SendTo(data, data.Length, SocketFlags.None, ipEP);
    }

    public string ReceivedMessages()
    {
        string recv = SerializeSystem.DeserializeData<string>(data);
        message = recv;
        return message;
    }

    public void SendMessages(string mess)
    {
        data = SerializeSystem.SerializeData(mess);
        socket.SendTo(data, data.Length, SocketFlags.None, ipEP);
    }

    public void Login(PlayerData playerData)
    {
        data = SerializeSystem.SerializeData(playerData);
        socket.SendTo(data, data.Length, SocketFlags.None, ipEP);
    }
}