﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public static class SerializeSystem
{
    public static byte[] SerializeData<T> (this T data)
    {
        using (MemoryStream memoryStream = new MemoryStream())
        {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Binder = new CustomBinder();
            formatter.Serialize(memoryStream, data);
            return memoryStream.ToArray();
        }
    }

    public static T DeserializeData<T> (this byte[] data)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Binder = new CustomBinder();
        using (MemoryStream memoryStream = new MemoryStream(data))
        {
            return (T)formatter.Deserialize(memoryStream);
        }
    }
}

sealed class CustomBinder : SerializationBinder
{
    public override Type BindToType(string assemblyName, string typeName)
    {
        Type returntype = null;
        string sharedAssemblyName = "SharedAssembly, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        assemblyName = Assembly.GetExecutingAssembly().FullName;
        typeName = typeName.Replace(sharedAssemblyName, assemblyName);
        returntype =
                Type.GetType(String.Format("{0}, {1}",
                typeName, assemblyName));

        return returntype;
    }

    public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
    {
        base.BindToName(serializedType, out assemblyName, out typeName);
        assemblyName = "SharedAssembly, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
    }
}