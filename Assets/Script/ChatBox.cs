﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ChatBox : MonoBehaviour
{
    [SerializeField] InputField message, username, password, ip, port;

    [SerializeField] Text text;

    [SerializeField] ClientScript clientScript;

    public PlayerData data;

    bool isConnect;

    private void Start()
    {
        isConnect = false;
    }

    public void SendMessages()
    {
        clientScript.SendMessages(message.text);
    }

    public void Login()
    {
        data = new PlayerData("", "");
        data.username = username.text;
        data.password = password.text;
        clientScript.Login(data);
    }

    public void ConnectToServer()
    {
        string pp = port.text;
        clientScript.ConnectToServer(ip.text, Int32.Parse(pp));
        isConnect = true;
    }

    private void Update()
    {
        if (isConnect)
            text.text = clientScript.ReceivedMessages();
    }
}
