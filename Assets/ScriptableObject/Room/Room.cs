﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = " TextBasedGames/Room"]

public class Room : ScriptableObject
{
    [text area]
    public string RoomName;
    public string description;

}
